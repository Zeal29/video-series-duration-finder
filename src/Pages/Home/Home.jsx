import React, {useState}  from 'react';
import {Container, Input} from "@material-ui/core";
import Typography         from "@material-ui/core/Typography";

import classesHome    from './Home.module.css'
import VideosTreeView from "../../Components/VideosTreeView/VideosTreeView";

let minToHms = (rawMins) => {
    let min = Math.floor(rawMins / 60);
    min += (min < 0) ? 1 : 0;
    let sec = Math.abs(rawMins % 60);
    sec     = (sec < 10) ? '0' + sec : sec;
    
    let h    = Math.floor(min / 60);
    h += (h < 0) ? 1 : 0;
    let min2 = Math.abs(min % 60);
    min2     = (min2 < 10) ? '0' + min2 : min2;
    
    return h + ':' + Math.floor(min2) + ':' + Math.floor(sec);
};

const Home = () => {
    const [treeViewData, setTreeViewData] = useState({});
    const [totalTime, setTotalTime]       = useState('00:00:00');
    
    window.URL = window.URL || window.webkitURL;
    
    let myVideos   = new Set();
    let videosData = [];
    
    const refreshTreeView = () => {
        
        let tree = {...treeViewData};
        
        const makeNesingKeys = (obj, video, keys, index = 0) => {
            const key = keys[index];
            
            if (keys.length - 1 == index)
            {
                let totalTime = 0;
                if (obj['__files'] == null)
                {
                    obj['__files'] = new Set();
                }
                
                if (!obj['__files'].has(video))
                {
                    totalTime = video.duration;
                }
                
                obj['__files'].add(video);
                
                
                return totalTime;
            }
            
            if (obj[key] == null)
            {
                obj[key] = {
                    __duration: 0,
                };
                obj[key].__duration += makeNesingKeys(obj[key], video, keys, index + 1);
                
                return obj[key].__duration;
            }
            else
            {
                obj[key].__duration += makeNesingKeys(obj[key], video, keys, index + 1);
                return obj[key].__duration;
            }
        };
        
        
        videosData.forEach(video => {
            const dirs    = video.fullPath.split('/');
            makeNesingKeys(tree, video, dirs);
        });
        
        setTreeViewData(tree);
        
        
        let totalTime = videosData.reduce((previousValue, currentValue) => previousValue + currentValue.duration, 0);
        setTotalTime(minToHms(totalTime));
        
        
    };
    
    function getfolder(e)
    {
        videosData = [];
        
        var files = Array.from(e.target.files).filter(
            file => file.type.split("/")[0] === "video"
        );
        
        files.forEach(files => {
            myVideos.add(files);
        });
        
        ////////////
        let count = myVideos.size;
        myVideos.forEach(videoFile => {
            let video     = document.createElement("video");
            video.preload = "metadata";
            
            video.onloadedmetadata = function () {
                window.URL.revokeObjectURL(video.src);
                videosData.push(
                    {
                        duration: video.duration,
                        name    : videoFile.name,
                        fullPath: videoFile.webkitRelativePath,
                    }
                );
                
                count--;
                if (count === 0)
                {
                    refreshTreeView();
                }
                // video.parentNode.removeChild(video);
            };
            
            video.src = URL.createObjectURL(videoFile);
        });
        ///////////
        
        console.log(myVideos);
    }
    
    
    return (
        <>
            <Container className={classesHome.Home} maxWidth="lg" >
                <Typography style={{marginTop: '40px'}} variant="h3" component="h1" >
                    Upload Folders
                </Typography >
                <p className="" >Select folders and see all the time of series</p >
                
                <input
                    type="file"
                    id="flup"
                    onChange={getfolder}
                    webkitdirectory="true"
                    mozdirectory="true"
                    msdirectory="true"
                    odirectory="true"
                    directory="true"
                    multiple
                />
                
                <h1 >Total Time {totalTime}</h1 >
                
                <VideosTreeView videoTreeViewData={treeViewData} />
            
            </Container >
        </>
    );
};

export default Home;
