import React         from 'react';

import './App.css';
import DefaultLayout from "./Layout/DefaultLayout";

function App() {
  return (
    <>
      <DefaultLayout/>
    </>
  );
}

export default App;
