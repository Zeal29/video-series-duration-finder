import {useState, useEffect} from 'react';


export default () => {
    const [counts, setCounts] = useState(-1);
    const [onDataLoadComplete, setOnDataLoadComplete] = useState( () => {});
    
    /**
     *
     * @param videosArray {Set}
     * @param videosDataArray  {Array}
     */
    const startReadingData = (videosArray, videosDataArray ,onDataLoadCompleteleCallback) => {
        setCounts(counts => videosArray.size);
        setOnDataLoadComplete(onDataLoadCompleteleCallback);
        
        videosArray.forEach(videoFile => {
            let video     = document.createElement("video");
            video.preload = "metadata";
            
            video.onloadedmetadata = function () {
                
                
                window.URL.revokeObjectURL(video.src);
                videosDataArray.push(
                    {
                        duration: video.duration,
                        name    : videoFile.name,
                        fullPath: videoFile.webkitRelativePath,
                    }
                )
            };
            
            video.src = URL.createObjectURL(videoFile);
            
            setCounts(state => {
               return  state - 1;
            });
            
        });
    };

    
    useEffect(()=>{
        if (counts === 0)
        {
            onDataLoadComplete();
        }
        
    },[counts]);
    
    
    
    return [startReadingData];
    
}
