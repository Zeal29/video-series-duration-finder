import React                from 'react';
import {TreeItem, TreeView} from "@material-ui/lab";
import Typography           from "@material-ui/core/Typography";
import VideocamIcon from '@material-ui/icons/Videocam';
import FolderIcon           from '@material-ui/icons/Folder';
import FolderOpenIcon       from '@material-ui/icons/FolderOpen';

import {makeStyles} from '@material-ui/core/styles';

import videoClasses from './VideosTreeView.module.css'


/**
 *
 * @type {StylesHook<Styles<Theme, {}, string>>}
 */



const useStyles = makeStyles(
    {
        root: {
            height   : 216,
            flexGrow : 1,
            maxWidth : 1000,
            marginTop: '40px'
        },
        
        treeViewContainer: {
            display: 'flex',
        },
        
        
        
        time: {
            'margin-left': '50px',
            'font-weight': '700',
            color        : 'gray'
        },
        
    });

let minToHm = (m) => {
    let h  = Math.floor(m / 60);
    h += (h < 0) ? 1 : 0;
    let m2 = Math.abs(m % 60);
    m2     = (m2 < 10) ? '0' + m2 : m2;
    return h + ':' + Math.floor(m2);
};

const VideosTreeView = ({videoTreeViewData}) => {
    const classes = useStyles();
    
    
    console.log(videoTreeViewData, 'Rendered')
    
    
    const fillTreeItems = (videoTreeViewData) => {
        
        const keys = Object.keys(videoTreeViewData);
        
        if (keys.length === 0)
        {
            return (
                <TreeItem nodeId="1" label={'No Folder is being Selected'} />
            );
        }
        
        
        let result = [];
        
        
        for (const key of keys)
        {
            if (key === '__duration')
            {
                continue;
            }
            
            if (key === '__files')
            {
                let arrayOfTreeItems = [];
                
                
                videoTreeViewData.__files.forEach(video => {
                    let divElement = (
                        <div className={videoClasses.treeViewContainer} >
                            <Typography variant="body2" >
                                {video.name}
                            </Typography >
                            <Typography className={videoClasses.time} variant="caption" color="inherit" >
                                {minToHm(video.duration)}
                            </Typography >
                        </div >
                    );
                    
                    arrayOfTreeItems.push(<TreeItem  key={video.name} nodeId={video.name + '_file'} label={divElement} />)
                });
                result.push(arrayOfTreeItems);
            }
            else
            {
                let treeItems = fillTreeItems(videoTreeViewData[key]);
                
                let divElement = (
                    <div className={videoClasses.treeViewContainerFolder} >
                        <Typography variant="body1" >
                            {key}
                        </Typography >
                        <Typography className={videoClasses.filderTime} variant="body2" color="inherit" >
                            {minToHm(videoTreeViewData[key].__duration)}
                        </Typography >
                    </div >
                );
                
                result.push(
                    <TreeItem key={key} nodeId={key} label={divElement} >
                        {treeItems}
                    </TreeItem >
                );
                
            }
            
            
        }
        
        return result;
    };
    
    let treeItems = fillTreeItems(videoTreeViewData);
    
    
    return (
        <TreeView
            className={classes.root}
            defaultCollapseIcon={< FolderOpenIcon />}
            defaultExpandIcon={<FolderIcon />}
            defaultEndIcon={<VideocamIcon />}
        >
            {treeItems}
        </TreeView >
    );
    
    
}
export default VideosTreeView;
